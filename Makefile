include ${FSLCONFDIR}/default.mk

PROJNAME = tbss

LIBS = -lfsl-newimage -lfsl-miscmaths -lfsl-cprob -lfsl-NewNifti -lfsl-znz -lfsl-utils

XFILES = tbss_skeleton swap_voxelwise swap_subjectwise

SCRIPTS = tbss_1_preproc tbss_2_reg tbss_3_postreg tbss_4_prestats fsl_reg tbss_non_FA tbss_fill tbss_3_postreg_new tbss_non_FA_new tbss_deproject tbss_sym tbss_x

all: ${XFILES}

%: %.cc
	${CXX} ${CXXFLAGS} -o $@ $^ ${LDFLAGS}
